from setuptools import setup, find_packages

version = '1.0.0.dev0'

install_requires = [
    'Django',
    'setuptools',
    'pkginfo',
    'django-compressor',
    'gunicorn', # the webserver in production
]

setup(
    name = "django-tutorial",
    version = "1.0",
    url = 'http://github.com//',
    license = 'BSD',
    description = "A short django start tutorial.",
    author = 'Michel van Leeuwen',
    install_requires = install_requires ,
)