Changelog of django buildout example
=================================


0.0.1 (unreleased)
------------------

- Setting up a buildout with Django 1.11.x (1.11.7 now)

- Splitted versions for buildout to versions.cfg

- Removed pacakge django-configurations (project is too old, does not work well with django 1.11)


