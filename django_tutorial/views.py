import logging
from django.views.generic import TemplateView
import os

from django.contrib import messages

logger = logging.getLogger(__name__)

class HomeView(TemplateView):
    template_name = "django_tutorial/home.html"

class SecondView(TemplateView):
    template_name = "django_tutorial/second_page.html"

class AnotherView(TemplateView):
    template_name = "django_tutorial/page_with_extra_context.html"

    def get_context_data(self, **kwargs):
        context = super(AnotherView, self).get_context_data(**kwargs)
        # Here you can add extra stuff to the context, we use this example:
        context['extra_something'] = 'Show how to use the context'
        django_config = None
        try:
            django_config=os.environ['DJANGO_CONFIGURATION']
        except Exception, e:
            message ='Failed to get the django config used: %s' %  e
            logger.error(message)
            if self.request:
                messages.error(self.request,message)
        context['django_config'] = django_config
        return context
