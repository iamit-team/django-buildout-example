# package
VERSION = None
__version__ = None

try:
    import pkg_resources
    __version__ = pkg_resources.get_distribution("dango_tutorial").version
    VERSION = __version__ # synonym
except Exception:
    pass