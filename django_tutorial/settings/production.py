from .base import *

import os

os.environ.setdefault('DJANGO_CONFIGURATION', 'Production')

DEBUG = False
ALLOWED_HOSTS = ['django-tutorial.iamit.nl', ]

# for development we use sqllite, choose other db server for production
# Note: for now we start with sql lite for production, database server in later step.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BUILDOUT_DIR, 'var', 'sqlite', 'db.sqlite3')
    }
    }
