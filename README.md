Example of a simple project build with buildout
===============================================

This project is to demonstrate the code behind this (short) tutorial on iamit.nl:

Note: this repo is ahead of the example on the website... (using django 1.11.7 now, also a LTS version)

[IAmIT Tips, Tricks and Tutroials: deploy a django site, using buildout](http://iamit.nl/content/django-buildout-new-project/)

see [Documentation](documentation/index.md)

Note: buildout is at the moment the way (Used by IAmIT) to deploy a django webapplication (and being able to run it locally, and deploy it with another environment on the server)

Another tool that's used often for django is for example 'fabric', or in combination with buildout.


Note:

I made a follow up in another example repo:


how to use apps in a project, 2 methods: embed an app, or a reusable app

see [https://bitbucket.org/iamit-team/django-site-with-apps-example](https://bitbucket.org/iamit-team/django-site-with-apps-example)


Added the [CHANGES.md](CHANGES.md), used for fullrelease (TODO: explanation)



