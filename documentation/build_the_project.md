[Back to doc index](index.md)

Build the project
==================

Buildout usage
--------------

Buitldout is a way to deploy our django sites.

After you have cloned this site, you can simply do some of these commands (in this order), and you will have an up and running site on your local pc.

The directory (dir) where the buildout.cfg file is, is called the 'buildout dir'.

Note: i assume everything is done command line, and performed on a linux machine (mac is also possible, where needed make a mac remark)

Setup buildout
---------

To set up buildout, I refer to: [buildout config local](http://iamit.nl/content/django-buidlout-new-project/)

Note: not nessecary, but recommended. In case running more projects, the buildout config takes care the package downloads will be done in the same folder


You can install buildout globally
pip install zc.buildout

or download the latest bootstrap:

[https://github.com/zestsoftware/zest.releaser/blob/master/bootstrap.py](https://github.com/zestsoftware/zest.releaser/blob/master/bootstrap.py)


For python 3, the best way is to install it globally:

pip3 install zc.buildout



Start a local working site
--------------------------

cd (change directory) in to the buildout dir

Once globally installed,

cd in to the buildout dir (where the buildout.cfg is) 

and just run:

buildout




for python 2.7:
--------------

run:

    buildout bootstrap

Note: for mac users see: [buildout on mac](buildout_on_mac.md)

Note, it will download the latest setup tools (at time of documentation setuptools-33.1.1)

We defined another version in buidlout, but this will only be recognised after running buildout, os another download will follow

You will se a bin dir with all kind of scripts. (no need to study them)


run this to build the project:

    bin/buildout

Using buildout means all 'python manage.py' commands are replaced by 'bin/django'

So in stead of using 'python manage.py migrate', we use:

    bin/django migrate

Migrate takes care of database migrations (later more info on that), we need to run it to fille the (local sqlite)
 database with the initial needed tables.


create a superuser:

    bin/django createsuperuser
    
Note: use you name and a simple password (it's just local developer usage)

and in stead of using 'python manage.py runserver', we use:

    bin/django runserver
    
After this, you can open the website in you browser, using this link:

[ http://127.0.0.1:8000/]( http://127.0.0.1:8000/)

it uses the default port 8000 to start the development django buildin web server

