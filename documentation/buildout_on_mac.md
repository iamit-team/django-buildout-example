[back to do index](index.md) - [back to buildout doc](build_the_project.md)

Notes to run buildout on MAC OS
===============================

Curl has some difficulties on the mac

So I included the ez_setup.py in the root and the zip for setuptools 33.1.1

Note that all packages I use can be found on: https://pypi.python.org/pypi

If you search for setuptools in there, it will redirect you to the latest (at time of documentation: 36.6.0)

https://pypi.python.org/pypi/setuptools/36.6.0

We cannot use this one, use 33.1.1

Just change the version in the one you need:

[https://pypi.python.org/pypi/setuptools/33.1.1](https://pypi.python.org/pypi/setuptools/33.1.1)

I already downloaded this one for you

CD in the buildout dir and install it:

pip install setuptools-33.1.1.zip

In stead of running: python bootstrap-buildout.py (you can check the options running: python bootstrap-buildout.py --help)

run python bootstrap-buildout.py --allow-site-packages

This way it will keep using the already downloaded and installed setuptools 33.1.1






